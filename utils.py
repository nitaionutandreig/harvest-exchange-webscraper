from datetime import datetime
import pyPdf

def check_date(date):
    return datetime.strptime(date, '%b %d, %Y')

def metadata_date(date):
    return date.strftime('%Y%m%d')
    
def valid_date(date):
    try:
        return datetime.strptime(date, "%Y%m%d")
    except ValueError:
        print "Not a valid date: '{0}'.".format(date)
        exit()

def extract_text(download_url, title, path):
    content = ""
    filename = title + '_SUPPLEMENT.txt'
    pdf = pyPdf.PdfFileReader(open(path + '/' + title + '_SUPPLEMENT.pdf', 'rb'))
    for i in range(0, pdf.getNumPages()):
        content += pdf.getPage(i).extractText() + "\n"
    f = open(path + '/' + filename, 'w')
    f.write(content.encode('UTF-8'))
    f.close()
    print 'Text extracted from PDF.'
    
