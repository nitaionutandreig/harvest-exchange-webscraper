import os, time
import csv
import argparse
import requests
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from utils import check_date, valid_date, extract_text, metadata_date

chrome_options = Options()  
chrome_options.add_argument("--headless")  


current_datetime = datetime.now()
login_url = 'https://www.hvst.com/access?callout_message=Access+the+latest+knowledge+and+insight+from+the+leading+investment+firms+in+one+place.'
email = 'nitaionutandrei@hotmail.com'
password = 'harvestexchange'

post_hrefs = []
metadata = {}

# user_urls = ['https://www.hvst.com/user/zach-kouwe']
# user_urls = ['https://www.hvst.com/organization/geoinvesting-llc']
# user_urls = ['https://www.hvst.com/user/j-kyle-bass']
# user_urls = ['https://www.hvst.com/organization/berylelites']
user_urls = ['https://www.hvst.com/organization/fidelity-institutional-asset-management']

def download_file(download_url, title, path):
    response = requests.get(download_url)
    filename = title + '_SUPPLEMENT.pdf'
    file = open(path + '/' + filename, 'wb')
    file.write(response.content)
    file.close()
    print "Saved file: {}".format(filename)

def parse_web_page(urls, start_date, end_date, path):
    driver = webdriver.Chrome(chrome_options=chrome_options)
    
    print ('Logging in')
    driver.get(login_url)
    time.sleep(5)
    driver.find_element_by_id('loginemail').send_keys(email)
    driver.find_element_by_xpath('//*[@id="signup_login"]/div[3]/div/div[2]/button').click()
    time.sleep(3)
    driver.find_element_by_id('user-password').send_keys(password)
    driver.find_element_by_xpath('//*[@id="signup_login"]/div[3]/div/div[3]/div/button').click()
    time.sleep(1)

    for url in urls:
        print 'Scraping URL: {}'.format(url)
        driver.get(url)

        print 'Checking for More button.'
        time.sleep(3)
        counter = 0
        more_button = driver.execute_script('return document.getElementsByClassName("mdl-button--primary")')
        while counter < 2 and more_button:
            more_button[0].click()
            time.sleep(5)
            more_button = driver.execute_script('return document.getElementsByClassName("mdl-button--primary")')
            counter += 1
        
        print 'Recovering all posts.'
        js_posts = driver.execute_script("return document.querySelectorAll('#tcl-mainbar > div > div.Feed > div:nth-child(2) > div');")

        for post in js_posts:
            date = post.find_element_by_css_selector("div.mdl-card__title.mdl-card--border > div:nth-child(2) > span").get_attribute('innerHTML')
            formatted_date = check_date(date)
            
            if formatted_date >= start_date and formatted_date <= end_date:
                href = post.find_element_by_css_selector('div:nth-child(3) > div > a').get_attribute('href')
                post_hrefs.append((formatted_date, href))


        if post_hrefs:
            print 'Creating directories and subdirectories.'
            export_path = path + '/{}/{:02d}/{:02d}/{}/'.format(current_datetime.year, current_datetime.month, current_datetime.day, current_datetime.strftime('%H%M%S'))
            os.makedirs(export_path)
            for date, url in post_hrefs:
                print 'Scraping post at {}'.format(url)
                driver.get(url)
                metadata[url] = {}
                metadata[url]['date'] = metadata_date(date)
                metadata[url]['title'] = driver.title
                title = url.split('/')[-1]

                if url.split('/')[3] == 'organization':
                    try:
                        organization = driver.execute_script("return document.querySelector('#tcl-mainbar > div > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > span > a:nth-child(3)').innerHTML;")
                    except:
                        organization = driver.execute_script("return document.querySelector('#tcl-mainbar > div > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > span > a').innerHTML;")
                    
                    if organization:
                        if 'react-text' in organization:
                            metadata[url]['organization'] = organization.split('-->')[3].split('<!--')[0]
                        else:
                            metadata[url]['organization'] = organization
                        metadata[url]['user'] = driver.execute_script("return document.querySelector('#tcl-mainbar > div > div.mdl-card.mdl-shadow--2dp > div:nth-child(2) > div:nth-child(2) > span > a').innerHTML;")
                    else:
                        metadata[url]['user'] = 'N/A'
                        organization = driver.execute_script("return document.querySelector('#tcl-mainbar > div > div.mdl-card.mdl-shadow--2dp > div:nth-child(2) > div:nth-child(2) > span > a').innerHTML;")
                        metadata[url]['organization'] = organization.split('-->')[3].split('<!--')[0]

                elif url.split('/')[3] == 'posts':
                    metadata[url]['user'] = driver.execute_script("return document.querySelector('#tcl-mainbar > div > div.mdl-card.mdl-shadow--2dp > div:nth-child(2) > div:nth-child(2) > span > a:nth-child(1)').innerHTML;")
                    try:
                        organization = driver.execute_script("return document.querySelector('#tcl-mainbar > div > div.mdl-card.mdl-shadow--2dp > div:nth-child(2) > div:nth-child(2) > span > a:nth-child(3)').innerHTML;")
                    except:
                        organization = driver.execute_script("return document.querySelector('#tcl-mainbar > div > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > span > span.no-decoration').innerHTML;")
                    metadata[url]['organization'] = organization.split('-->')[3].split('<!--')[0]
                
                html_source = (driver.page_source).encode('utf-8')
                post_text = driver.execute_script(" return document.getElementById('post-content').innerHTML;")
                post_text = (post_text).encode('utf-8')
                export_html = open(export_path + title + '.html', 'w+')
                export_html.write(html_source)
                export_text = open(export_path + title + '.txt', 'w+')
                export_text.write(post_text)
                pdf_file = driver.execute_script("return document.getElementsByClassName('mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect text-center no-underline')[0]")
                
                metadata[url]['report_url'] = driver.current_url
                
                if pdf_file:
                    'Saving Supplement PDF file and text.'
                    supplement_url = pdf_file.get_attribute('href')
                    download_file(supplement_url, title, export_path)
                    extract_text(supplement_url, title, export_path) 
                    metadata[url]['supplement_url'] = supplement_url
                else:
                    metadata[url]['supplement_url'] = 'N/A'
                
                for key in metadata[url]:
                    metadata[url][key] = (metadata[url][key]).encode('utf-8')
                
                print 'Saving metadata.'
                with open(export_path + title + '.metadata.csv', 'w') as csvfile:
                    spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    spamwriter.writerow(["Publish Date", "Title", "User", "Organization", "Report URL", "Supplement URL"])
                    spamwriter.writerow([metadata[url]['date'], metadata[url]['title'], metadata[url]['user'], metadata[url]['organization'], metadata[url]['report_url'], metadata[url]['supplement_url']])
        else:
            print 'No posts within that date.'
            driver.close()
            exit()

        time.sleep(10)
        print 'Closing webpage.'

def main():
    print 'Starting webscraping script\n' + '-' * 150
    #Initializing parser
    parser = argparse.ArgumentParser(description='Webscraper for https://www.pershingsquareholdings.com')
    parser.add_argument('-s', "--start_date", help="Expected format: YYYYMMDD (Inclusive)", 
                        required=True, type=valid_date)
    parser.add_argument('-e', "--end_date", help="Expected format: YYYYMMDD (Inclusive)", 
                        required=True, type=valid_date)
    parser.add_argument('-o', "--output_folder", \
                        help='Directory where output needs to be stored', 
                        required=True)
    
    args = parser.parse_args()

    parse_web_page(user_urls, args.start_date, args.end_date, args.output_folder)


if __name__ == '__main__':
    main()